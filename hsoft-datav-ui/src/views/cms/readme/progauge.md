# 进度仪表盘

## 一、配置

### 1、图层

选中该仪表盘组件，在操作界面右侧的“图层名称”处修改组件图层的名称。建议设置，该内容和左侧控制面板该图表插件名称一致，便于后期图表多时管理；
  <viewer>
    <img src="../image/progauge.png" width="80%">
  </viewer>

### 2、标题

选中该仪表盘组件，在操作界面右侧的“标题”处可修改该组件的标题。

- 标题：组件中心标题显示的内容，如果不想显示标题，则不填写；

- 字体：标题字体名称；

- 字体大小：标题字体大小；

- 字体粗细：标题字体粗细，「可设置normal」「bold」「bolder」「lighter」；

- 字体颜色：标题字体颜色；

- 标题上边距：标题位置与图表上侧距离百分比；

- 标题左边距：标题位置与图表左侧距离百分比；

  例如设置标题为统计，下图设置标题字体为黑体，大小为40，粗细为normal，颜色为白色，边距分别为0,0和54,44

  <viewer>
    <img src="../image/progauge-2.png" width="80%">
  </viewer>
          

### 3、表盘

选中该仪表盘组件，在操作界面右侧的“表盘”处可修改该组件表盘有关的属性设置。

- 表盘宽度：表盘进度条宽度，如下图分别设置10和20；

  <viewer>
    <img src="../image/progauge-3.png" width="80%">
  </viewer>

- 表盘颜色：表盘进度条起始和终止渐变颜色；

- 剩余颜色：表盘进度条剩余百分比颜色；

  <viewer>
    <img src="../image/progauge-4.png" width="80%">
  </viewer>

### 4、刻度

选中该仪表盘组件，在操作界面右侧的“刻度”处可修改该组件刻度有关的属性设置。

- 刻度数量：设置表盘刻度总个数，刻度数「30」「50」对比如下;

  <viewer>
    <img src="../image/progauge-5.png" width="80%">
  </viewer>

- 刻度宽度：设置表盘刻度的宽度，刻度宽度「2」「8」对比如下;

  <viewer>
    <img src="../image/progauge-6.png" width="80%">
  </viewer>

- 刻度颜色：设置表盘刻度的颜色;

  <viewer>
    <img src="../image/progauge-7.png" width="80%">
  </viewer>

### 5、数值

选中该仪表盘组件，在操作界面右侧的“刻值”处可修改仪表盘组件中间数字的属性设置。

- 显示数值开关：是否显示数值；
  <viewer>
    <img src="../image/progauge-8.png" width="80%">
  </viewer>

- 字体：数值字体名称；

- 字体大小：数值字体大小；

- 字体粗细：数值字体粗细，可设置「normal」「bold」「bolder」「lighter」；

- 字体颜色：数值字体颜色；

- 数值上边距：数值位置与图表上侧距离百分比；

- 数值左边距：数值位置与图表左侧距离百分比；

  例如设置数值字体分别为Digital、黑体，字体大小59、65，字体粗细normal、bold，字体颜色蓝色、绿色，边距[3,-18]、[0,0]

  <viewer>
    <img src="../image/progauge-9.png" width="80%">
  </viewer>

### 6、圆环

选中该仪表盘组件，在操作界面右侧的“圆环”处可修改仪表盘组件圆环的属性设置。

- 外环内半径：外圈圆环内半径大小；

- 外环外半径：外圈圆环外半径大小；

- 外环颜色：外圈圆环颜色；

  <viewer>
    <img src="../image/progauge-10.png" width="80%">
  </viewer>

- 内环内半径：里圈圆环内半径大小；

- 内环外半径：里圈圆环外半径大小；

- 内环颜色：里圈圆环颜色；

  <viewer>
    <img src="../image/progauge-11.png" width="80%">
  </viewer>

### 7、动画

选中该组件，在操作界面右侧的“动画”处可修改组件的载入动画效果。分别为：

- 弹跳

- 渐隐渐显

- 向右滑入

- 向左滑入

- 放缩

- 旋转

- 滚动

## 二、数据

静态数据格式：

```
 87
```
