import request from '@/utils/request'

// 查询社区问答列表
export function listQuestion(query) {
  return request({
    url: '/datav/question/list',
    method: 'get',
    params: query
  })
}

// 查询社区问答详细
export function getQuestion(id) {
  return request({
    url: '/datav/question/' + id,
    method: 'get'
  })
}

// 新增社区问答
export function addQuestion(data) {
  return request({
    url: '/datav/question',
    method: 'post',
    data: data
  })
}

// 修改社区问答
export function updateQuestion(data) {
  return request({
    url: '/datav/question',
    method: 'put',
    data: data
  })
}

// 删除社区问答
export function delQuestion(id) {
  return request({
    url: '/datav/question/' + id,
    method: 'delete'
  })
}

// 导出社区问答
export function exportQuestion(query) {
  return request({
    url: '/datav/question/export',
    method: 'get',
    params: query
  })
}

// 问答列表
export function getQuestionList(query) {
  return request({
    url: '/datav/question/list/user',
    method: 'get',
    params: query
  })
}

// 查看数+1
export function viewCountIncrease(data) {
  return request({
    url: '/datav/question/viewCountIncrease',
    method: 'post',
    data: data
  })
}

// 最新回复列表
export function getLatestReply() {
  return request({
    url: '/datav/question/reply/latest',
    method: 'get'
  })
}