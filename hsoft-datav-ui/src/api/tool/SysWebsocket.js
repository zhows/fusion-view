import request from '@/utils/request'

// 查询websocket通讯接口管理列表
export function listSysWebsocket(query) {
  return request({
    url: '/system/SysWebsocket/list',
    method: 'get',
    params: query
  })
}

// 查询websocket通讯接口管理详细
export function getSysWebsocket(id) {
  return request({
    url: '/system/SysWebsocket/' + id,
    method: 'get'
  })
}

// 新增websocket通讯接口管理
export function addSysWebsocket(data) {
  return request({
    url: '/system/SysWebsocket',
    method: 'post',
    data: data
  })
}

// 修改websocket通讯接口管理
export function updateSysWebsocket(data) {
  return request({
    url: '/system/SysWebsocket',
    method: 'put',
    data: data
  })
}

// 删除websocket通讯接口管理
export function delSysWebsocket(id) {
  return request({
    url: '/system/SysWebsocket/' + id,
    method: 'delete'
  })
}

// 导出websocket通讯接口管理
export function exportSysWebsocket(query) {
  return request({
    url: '/system/SysWebsocket/export',
    method: 'get',
    params: query
  })
}